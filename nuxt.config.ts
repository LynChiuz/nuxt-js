// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: true,
  css: ['@/dist/css/app.css'],
  devtools: { enabled: true },

  
})
